﻿using ImageMagick;
using Microsoft.Extensions.Primitives;

namespace API.Logic.Transformers;

public class FormatTransformer : AbstractTransformer
{
    protected override List<string> GetQueryStringKeys()
    {
        return ["format"];
    }

    protected override MagickImage Handle(MagickImage magickImage, Dictionary<string, StringValues> parameters)
    {
        var queryParameterValue = parameters.Values.First().First();
        
        magickImage.Format = queryParameterValue switch
        {
            "jpg" => MagickFormat.Jpeg,
            "png" => MagickFormat.Png,
            "pjpg" => MagickFormat.Pjpeg,
            "webp" => MagickFormat.WebP,
            "avif" => MagickFormat.Avi,
            "gif" => MagickFormat.Gif,
            _ => magickImage.Format
        };

        return magickImage;
    }
}