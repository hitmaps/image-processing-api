﻿using ImageMagick;
using Microsoft.Extensions.Primitives;

namespace API.Logic.Transformers;

public class OrientationTransformer : AbstractTransformer
{
    protected override List<string> GetQueryStringKeys()
    {
        return ["orientation"];
    }

    protected override MagickImage Handle(MagickImage magickImage, Dictionary<string, StringValues> parameters)
    {
        var queryParameterValue = parameters.Values.First().First();
        
        if (double.TryParse(queryParameterValue, out var rotationDegrees))
        {
            magickImage.Rotate(rotationDegrees);
        } else if (queryParameterValue == "auto")
        {
            magickImage.AutoOrient();
        }
        
        return magickImage;
    }
}