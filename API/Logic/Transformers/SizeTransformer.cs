﻿using System.Diagnostics.CodeAnalysis;
using ImageMagick;
using Microsoft.Extensions.Primitives;

namespace API.Logic.Transformers;

public class SizeTransformer : AbstractTransformer
{
    protected override List<string> GetQueryStringKeys()
    {
        return ["width", "height"];
    }

    protected override MagickImage Handle(MagickImage magickImage, Dictionary<string, StringValues> parameters)
    {
        uint targetWidth = 0;
        uint targetHeight = 0;

        #region Width
        if (parameters.TryGetValue("width", out var width))
        {
            targetWidth = uint.Parse(width[0]!);
        }
        #endregion
        #region Height
        if (parameters.TryGetValue("height", out var height))
        {
            targetHeight = uint.Parse(height[0]!);
        }
        
        var size = new MagickGeometry(targetWidth, targetHeight);
        #endregion
        
        magickImage.Resize(size);
        
        return magickImage;
    }
}