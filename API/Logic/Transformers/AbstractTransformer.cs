﻿using ImageMagick;
using Microsoft.Extensions.Primitives;

namespace API.Logic.Transformers;

public abstract class AbstractTransformer
{
    private bool AppliesTo(string queryStringKey)
    {
        return GetQueryStringKeys().Contains(queryStringKey);
    }

    protected abstract List<string> GetQueryStringKeys();

    public MagickImage Transform(MagickImage magickImage, Dictionary<string, StringValues> queryParameters)
    {
        var applicableParameters = queryParameters
            .Where(keyValuePair => AppliesTo(keyValuePair.Key))
            .ToDictionary(keyValuePair => keyValuePair.Key, keyValuePair => keyValuePair.Value);

        return applicableParameters.Count == 0 ? magickImage : Handle(magickImage, applicableParameters);
    }
    
    protected abstract MagickImage Handle(MagickImage magickImage, Dictionary<string, StringValues> parameters);
}