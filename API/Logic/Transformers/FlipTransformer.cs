﻿using ImageMagick;
using Microsoft.Extensions.Primitives;

namespace API.Logic.Transformers;

public class FlipTransformer : AbstractTransformer
{
    protected override List<string> GetQueryStringKeys()
    {
        return ["flip"];
    }

    protected override MagickImage Handle(MagickImage magickImage, Dictionary<string, StringValues> parameters)
    {
        var queryParameterValue = parameters.Values.First().First();
        
        if (queryParameterValue is "v" or "both")
        {
            magickImage.Flip();
        }
        
        if (queryParameterValue is "h" or "both")
        {
            magickImage.Flop();
        }
        
        return magickImage;
    }
}