﻿using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using API.Logic.Transformers;
using ImageMagick;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;

namespace API;

[ApiController]
public class MainController : Controller
{
    private readonly IConfiguration configuration;
    private readonly List<AbstractTransformer> filters;

    public MainController(IConfiguration configuration)
    {
        this.configuration = configuration;
        filters = GetTransformers();
    }

    private static List<AbstractTransformer> GetTransformers()
    {
        return Assembly.GetAssembly(typeof(AbstractTransformer))!
            .GetTypes()
            .Where(t => t is { IsClass: true, IsAbstract: false } && t.IsSubclassOf(typeof(AbstractTransformer)))
            .Select(type => (AbstractTransformer)Activator.CreateInstance(type)!)
            .ToList();
    }

    [HttpGet]
    [Route("/{**path}")]
    public IActionResult Get(string path)
    {
        if (path.Contains("..") || path.Contains("favicon.ico"))
        {
            return NotFound();
        }
        
        var queryParams = Request.Query.ToDictionary(kvp => kvp.Key, kvp => kvp.Value);
        if (MultipleValuesForSameParameter(queryParams))
        {
            return BadRequest(new
            {
                Message = "Only one of each parameter is allowed."
            });
        }

        if (!queryParams.ContainsKey("format") && BrowserSupportsWebp(Request))
        {
            queryParams.Add("format", "webp");
        }
        
        // Do we have a cached copy?
        var cachedImage = GetCachedImage(path, queryParams);
        if (cachedImage != null)
        {
            var cachedImageBytes = System.IO.File.ReadAllBytes(cachedImage);
            return File(cachedImageBytes, MagickFormatInfo.Create(cachedImageBytes)!.MimeType!);
        }
     
        using var image = new MagickImage(new FileInfo(Path.Join(configuration["MediaLibraryRootPath"], path)));
        
        filters.ForEach(filter => filter.Transform(image, queryParams));
        
        var data = image.ToByteArray();

        CacheImage(path, queryParams, data);

        return File(data, MagickFormatInfo.Create(image.Format)!.MimeType!);
    }

    private string? GetCachedImage(string path, Dictionary<string, StringValues> queryParams)
    {
        if (configuration["MediaLibraryCachePath"] == null)
        {
            return null;
        }
        
        var cachedPath = Path.Join(configuration["MediaLibraryCachePath"], path, GenerateCachedFilename(path, queryParams));

        return new FileInfo(cachedPath).Exists ? cachedPath : null;
    }

    private static bool MultipleValuesForSameParameter(Dictionary<string, StringValues> query)
    {
        return query.Keys.Any(x => query[x].Count > 1);
    }

    private static bool BrowserSupportsWebp(HttpRequest request)
    {
        return request.Headers.Accept.Any(x => x != null && x.Contains("image/webp"));
    }

    private static string GenerateCachedFilename(string path, Dictionary<string, StringValues> query)
    {
        var queryParts = query
            .Select(entry => $"{entry.Key}={entry.Value}")
            .ToList();
        return BitConverter.ToString(MD5.HashData(Encoding.UTF8.GetBytes($"{path}?{string.Join("&", queryParts)}")))
            .Replace("-", string.Empty)
            .ToLower();
    }

    private void CacheImage(string path, Dictionary<string, StringValues> queryParams, byte[] data)
    {
        if (configuration["MediaLibraryCachePath"] == null)
        {
            return;
        }

        
        // Write to .lock file first
        var cachedFilename = Path.Join(configuration["MediaLibraryCachePath"], path,
            GenerateCachedFilename(path, queryParams));

        if (!Directory.Exists(Path.Join(configuration["MediaLibraryCachePath"], path)))
        {
            Directory.CreateDirectory(Path.Join(configuration["MediaLibraryCachePath"], path));
        }
        System.IO.File.WriteAllBytes($"{cachedFilename}.lock", data);
        
        System.IO.File.Move($"{cachedFilename}.lock", cachedFilename);
    }
}